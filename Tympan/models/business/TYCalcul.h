/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/*
 *
 */

/* ==============================================================================
    TO DO :
            1. Save _geomPrecision in XML
    ============================================================================= */

#ifndef __TY_CALCUL__
#define __TY_CALCUL__

#include <memory>

#include "Tympan/models/business/infrastructure/TYSiteNode.h"
#include "Tympan/models/business/TYRay.h"
#include "Tympan/models/business/TYMaillage.h"
#include "Tympan/models/business/TYPointControl.h"
#include "Tympan/models/business/TYResultat.h"

class TYProjet;

/**
 * \file TYCalcul.h
 * \class TYCalcul
 * \version v 1.2
 * \brief Calculation program.
 * \author Projet_Tympan
 * \date 2008/01/25 14:06:42
 *
 * The program that defines the Calculations.
 *
 */
class TYCalcul : public TYElement
{
    OPROTODECL(TYCalcul)
    TY_EXTENSION_DECL_ONLY(TYCalcul)
    TY_EXT_GRAPHIC_DECL_ONLY(TYCalcul)

public:
    /**
     * \fn enum CalculState
     * \brief The different possible states for a calculation.
     */
    enum CalculState
    {
        Locked, /**< The calculation cannot be executed (modified). */
        Actif,  /**< The calculation can be executed (modified). */
    };

    TYCalcul();

    ///**
    // * \brief build a calcul giving his parent.
    // *  Constructor of the TYCalcul class.
    // *  \param LPTYProjet  pParent=Nulle
    // */
    // TYCalcul(LPTYProjet pParent);

    /**
     *\brief Constructor by copy.
     *\param other
     * Constructor by copying the TYCalcul class.
     */
    TYCalcul(const TYCalcul& other);

    /**
     *\brief Destructor.
     * Destructor of the TYCalcul class
     */
    virtual ~TYCalcul();

    /// Operator =.
    TYCalcul& operator=(const TYCalcul& other);
    /// Operator ==.
    bool operator==(const TYCalcul& other) const;
    /// Operator !=.
    bool operator!=(const TYCalcul& other) const;

    /**
     * Performs a copy by allocating memory (deep copy) and
     * not by copying only the pointers (shallow copy).
     * This method is used as the equal operator.
     * ID may not be copied, parent is never copied.
     *
     * @param pOther The element to copy.
     * @param copyId Copy unique id or not.
     * @param pUseCopyTag copy tag usage
     *
     * @return <code>true</code> if copying is possible;
     * <code>false</code> otherwise.
     */
    virtual bool deepCopy(const TYElement* pOther, bool copyId = true, bool pUseCopyTag = false);

    virtual std::string toString() const;

    virtual DOM_Element toXML(DOM_Element& domElement);
    virtual int fromXML(DOM_Element domElement);

    /*!
     * \brief Get the status of a point for this calcul
     */
    bool getPtCtrlStatus(const TYUUID& id_pt);

    /*!
     * \brief Set the status of a point for this calcul
     */
    void setPtCtrlStatus(const TYUUID& id_pt, bool bStatus);

    /**
     * \fn void purge()
     * \brief Reset this calculation.
     */
    void purge();

    /**
     * Empty the result of this calculation
     */
    void clearResult();

    /**
     * Returns the parent project.
     */
    TYProjet* getProjet();

    /**
     * \fn int getNumero()
     * \brief Get the number
     * \return _numero
     */
    int getNumero() const
    {
        return _numero;
    }

    /**
     * \fn void setNumero(int num)
     * \brief Set the number
     */
    void setNumero(int num)
    {
        _numero = num;
    }

    /**
     * \fn QString getAuteur()
     * \brief Get zuthor name
     * \return _auteur
     */
    QString getAuteur() const
    {
        return _auteur;
    }

    /**
     * \fn void setAuteur(QString auteur)
     * \brief Set author name
     */
    void setAuteur(QString auteur)
    {
        _auteur = auteur;
    }

    /**
     * fn QString getDateCreation()
     * void setDateCreation(const QString& date)
     * \brief Set/Get of creation date.
     * \return _dateCreation
     */
    QString getDateCreation() const
    {
        return _dateCreation;
    }
    void setDateCreation(const QString& date)
    {
        _dateCreation = date;
    }

    /**
     * fn QString getDateModif()
     * \brief Get modification date.
     * \return _dateModif
     */
    QString getDateModif() const
    {
        return _dateModif;
    }

    /**
     * fn void setDateModif(const QString& date)
     * \brief Set modification date.
     */
    void setDateModif(const QString& date)
    {
        _dateModif = date;
    }

    /**
     * \fn QString getComment()
     * \brief Get comments.
     * \return _comment
     */
    QString getComment() const
    {
        return _comment;
    }

    /**
     * \fn void setComment(const QString& comment)
     * \brief Set comments.
     */
    void setComment(const QString& comment)
    {
        _comment = comment;
    }

    /**
     * \fn  bool getIsUpTodate()
     * \brief Get the calculated state up to date or not link to the infrastructure.
     * \return _upTodate
     */
    bool getIsUpTodate() const
    {
        return _upTodate;
    }

    /**
     * \fn  void setIsUpTodate(bool upTodate)
     * \brief Set the calculated state up to date or not link to the infrastructure.
     */
    void setIsUpTodate(bool upTodate)
    {
        _upTodate = upTodate;
    }

    /**
     *\fn TYListID getElementSelection()
     *\brief Get the selection of active elements in this Calculation.
     *\return _elementSelection
     */
    TYListID getElementSelection() const
    {
        return _elementSelection;
    }

    /**
     * \fn bool addToSelection(TYUUID id)
     * \brief Adds the item to the selection of this Calculation.
     */
    bool addToSelection(TYUUID id);
    /**
     * \fn bool addToSelection(TYElement * pElt, bool recursif = true)
     * \brief Adds the item to the selection of this Calculation.
     */
    void addToSelection(TYElement* pElt, bool recursif = true);

    /**
     * \fn bool remToSelection(TYUUID id)
     * \brief Removes the item from the selection of this Calculation.
     */
    bool remToSelection(TYUUID id);
    /**
     * \fn bool remToSelection(TYElement * pElt, bool recursif = true)
     * \brief Removes the item from the selection of this Calculation.
     */
    bool remToSelection(TYElement* pElt, bool recursif = true);

    /**
     * \fn bool isInSelection(TYUUID id)
     * \brief Tests if the element is present in the selection of this Calculation.
     */
    bool isInSelection(TYUUID id);

    /**
     * \fn bool isInSelection(LPTYElement pElt)
     * \brief Tests if the element is present in the selection of this Calculation.
     */
    bool isInSelection(LPTYElement pElt)
    {
        assert(pElt);
        return isInSelection(pElt->getID().toString());
    }

    /**
     * \fn  TYMapPtrElementBool& getEmitAcVolNode()
     * \brief Get array containing the state of each volumeNode in transmission.
     * \return _emitAcVolNode
     */
    TYMapPtrElementBool& getEmitAcVolNode()
    {
        return _emitAcVolNode;
    }
    /**
     * \fn  const TYMapPtrElementBool& getEmitAcVolNode() const
     * \brief Get array containing the state of each volumeNode in transmission.
     * \return _emitAcVolNode
     */
    const TYMapPtrElementBool& getEmitAcVolNode() const
    {
        return _emitAcVolNode;
    }

    /**
     * \fn TYMapPtrElementInt& getMapElementRegime()
     * \brief Get from the regime
     * \return _mapElementRegime
     */
    TYMapPtrElementInt& getMapElementRegime()
    {
        return _mapElementRegime;
    }

    /**
     * \fn const TYMapPtrElementInt& getMapElementRegime()const
     * \brief Get regime tab
     * \return _mapElementRegime
     */
    const TYMapPtrElementInt& getMapElementRegime() const
    {
        return _mapElementRegime;
    }

    /*!
        \brief Add a checkpoint to the results array
        \param pPoint: Pointer to the point to add
     */
    bool addPtCtrlToResult(LPTYPointControl pPoint);

    /*!
        \brief Deletes a checkpoint from the results table
        \param pPoint: Pointer to the point to delete
     */
    bool remPtCtrlFromResult(LPTYPointControl pPoint);

    /*!
     * Get the spectrum for a given control point
     */
    LPTYSpectre getSpectre(const TYUUID& id_pt);
    LPTYSpectre getSpectre(const TYPointControl* pPoint);

    /*!
     * Set the spectrum for a given control point
     */
    void setSpectre(const TYUUID& id_pt, TYSpectre* pSpectre);
    void setSpectre(TYPointCalcul* pPoint, TYSpectre* pSpectre);

    /**
     * \fn  LPTYResultat getResultat() const
     * \brief Get result.
     * \return _pResultat
     */
    const LPTYResultat getResultat() const
    {
        return _pResultat;
    }

    /**
     * \fn  LPTYResultat getResultat()
     * \brief Get result.
     * \return _pResultat
     */
    LPTYResultat getResultat()
    {
        return _pResultat;
    }

    /**
     * \fn void getCalculElements(LPTYSiteNode pSite)
     * \brief Recover all the elements of the scene which take part in the calculation.
     * \param pSite Site from which the selection is made.
     */
    void getCalculElements(LPTYSiteNode pSite);

    /**
     * \fn void setSite(LPTYSiteNode pSite)
     * \brief Definition of the site on which the calculation will be done
     */
    void setSite(LPTYSiteNode pSite);

    /**
     * \fn LPTYSiteNode getSite()
     * \brief Get calculation site
     * \return _pSiteCalcul
     */
    LPTYSiteNode getSite();

    /**
     * \fn void setState(int state)
     * \brief Set editable attribute
     */
    void setState(int state)
    {
        _state = state;
    }

    /**
     * \fn int getState()
     * \brief Get calculation state
     * \return _state
     */
    int getState()
    {
        return _state;
    }

    /**
     * \fn const int getState()const
     * \brief Get calculation state
     * \return _state
     */
    const int getState() const
    {
        return _state;
    }

    /**
     * \fn bool getStatusPartialResult()const
     * \brief Get conservation of partial results
     * \return _pResultat->getPartialState()
     */
    bool getStatusPartialResult() const
    {
        return _pResultat->getPartialState();
    }

    /**
     * \fn void setStatusPartialResult(const bool& status)
     * \brief Partial result retention set
     */
    void setStatusPartialResult(const bool& status)
    {
        _pResultat->setPartialState(status);
    }

    /**
     * \fn void setSolverId(const OGenID& iD)
     * \brief Set solver ID
     * \return void
     */
    void setSolverId(const OGenID& iD)
    {
        _solverId = iD;
    }

    /**
     * \fn OGenID getSolverId() const
     * \brief Get solver ID
     * \return _solverId
     */
    const OGenID getSolverId() const
    {
        return _solverId;
    }

    /**
     * \fn OGenID getSolverId()
     * \brief Get solver ID
     * \return _solverId
     */
    OGenID getSolverId()
    {
        return _solverId;
    }

    void goPostprocessing();
    // Solver parameters for this computation
    QString solverParams;

    /**
     * \brief Returns an array containing all the rays found by ray tracing.
     * \return Returns the Tympanum ray array
     */
    TYTabRay& getTabRays()
    {
        return _tabRays;
    }

    /**
     * @brief Return spectrums for a given noise map
     */
    std::vector<LPTYSpectre>* getSpectrumDatas(const TYUUID& id);

    /**
     * @brief Return spectrums for a given noise map
     */
    std::vector<LPTYSpectre>* getSpectrumDatas(TYMaillage* pMaillage);

    /*!
     * \brief Add this maillage to calcul
     * \fn bool addMaillage(TYMaillage* pMaillage);
     */
    bool addMaillage(TYMaillage* pMaillage);

    /*!
     * \brief Remove a maillage from calcul
     * \fn bool remMaillage(TYMaillage* pMaillage);
     */
    bool remMaillage(TYMaillage* pMaillage);

    /*!
     * \brief update a noisemap after modification
     */
    bool updateMaillage(TYMaillage* pMaillage);

    /*!
     * \brief set spectrum vector for a given noise map
     * \fn void setNoiseMapSpectrums(const TYMaillage* pMaillage, TYTabLPSpectre& tabSpectrum);
     */
    void setNoiseMapSpectrums(const TYMaillage* pMaillage, TYTabLPSpectre& tabSpectrum);

    /*!
     * \brief set spectrum vector for a given noise map
     * \fn void setNoiseMapSpectrums(const TYUUID& id, TYTabLPSpectre& tabSpectrum);
     */
    void setNoiseMapSpectrums(const TYUUID& id, TYTabLPSpectre& tabSpectrum);

    /*!
     * \brief Returns map of control points with spectrum
     * \fn TYMapIdSpectre getMapPointCtrlSpectre();
     */
    TYMapIdSpectre getMapPointCtrlSpectre();

    /*!
     * \brief Method used for migrating T310 project
     * \ returns true if ResuCtrlPnts exists in xml project else returns false
     * \fn bool hasResuCtrlPoints()
     */
    bool hasResuCtrlPoints()
    {
        return _hasResuCtrlPnts;
    }

private:
    void clearCtrlPointsSpectrums();
    void clearNoiseMapsSpectrums();
    void copyNoiseMapSpectrums(TYMapIdTabSpectre& otherNoiseMap);

    // Members
protected:
    // solver to be used to solve this "calcul"
    OGenID _solverId;

    /// Calculation number.
    int _numero;
    /// Author name
    QString _auteur;
    /// Creation date
    QString _dateCreation;
    /// Modification date
    QString _dateModif;
    /// Comments
    QString _comment;

    /// Calculation up to date or not
    bool _upTodate;
    /// Calculation state Active or Blocked
    int _state;

    /// Site on which the calculation will be carried out
    LPTYSiteNode _pSiteCalcul;

    /// Array of IDs of elements present in the scene.
    TYListID _elementSelection;

    /// State (radiating/non-radiating) of the elements of the scene.
    TYMapPtrElementBool _emitAcVolNode;

    /// Regime of scene elements
    TYMapPtrElementInt _mapElementRegime;

    // Map control point with spectrum
    TYMapIdSpectre _mapPointCtrlSpectre;

    // Map NoiseMap Id with spectrum list
    TYMapIdTabSpectre _noiseMapsSpectrums;

    /// Results
    LPTYResultat _pResultat;

    // Valid rays produced by ray tracing
    TYTabRay _tabRays;

    // Indicator used for migrating T310 project
    // True if ResuCtrlPnts exists in xml project else false
    bool _hasResuCtrlPnts;
};

#endif // __TY_CALCUL__
