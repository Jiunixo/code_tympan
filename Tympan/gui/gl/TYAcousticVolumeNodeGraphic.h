/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * \file TYAcousticVolumeNodeGraphic.h
 * \brief Representation graphique d'un ensemble de volumes acoustiques (fichier header)
 * \author Projet_Tympan
 *
 *
 *
 *
 *
 */

#ifndef __TY_ACOUSTIC_VOLUME_NODE_GRAPHIC__
#define __TY_ACOUSTIC_VOLUME_NODE_GRAPHIC__

#if _MSC_VER > 1000
    #pragma once
#endif // _MSC_VER > 1000

#include "TYElementGraphic.h"
class TYAcousticVolumeNode;

/**
 * \class TYAcousticVolumeNodeGraphic
 * \brief classe graphique pour un ensemble de volumes acoustiques
 */
class TYAcousticVolumeNodeGraphic : public TYElementGraphic
{
    TY_DECL_METIER_GRAPHIC(TYAcousticVolumeNode)

    // Methodes
public:
    /**
     * Constructor
     */
    TYAcousticVolumeNodeGraphic(TYAcousticVolumeNode* pElement);

    /**
     * Destructor
     */
    virtual ~TYAcousticVolumeNodeGraphic() {}

    /**
     * Updates the 3D object associated to this object
     * Note : When this method is overloaded, the parent implementation
     * (TYElementGraphic::update()) must be called at the end
     *
     * @param force overstep the state of the flag 'modified' and apply the update.
     */
    virtual void update(bool force = false);

    /**
     * Effectue le trace openGL de l'objet graphique
     *
     * @param pModelerElement element du modeleur a afficher
     * @param mode Gere les modes rendu et select pour le picking.
     */
    virtual void display(TYElement* pModelerElement = nullptr, GLenum mode = GL_RENDER);

    /**
     * Store each child of the box in childs. If recursif is true, also store the children
     * of each child
     */
    virtual void getChilds(TYListPtrTYElementGraphic& childs, bool recursif = true);

    /**
     * Build the bounding box, with the min and max of each coordinate
     */
    virtual void computeBoundingBox();
};

#endif // __TY_ACOUSTIC_VOLUME_NODE_GRAPHIC__
