/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * \file TYLineEdit.h
 * \brief outil IHM pour une entrée utilisateur (fichier header)
 * \author Projet_Tympan
 *
 */

#include <QKeyEvent>

#include "Tympan/gui/widgets/TYFormDialog.h"
#include "TYLineEdit.h"

TYLineEdit::TYLineEdit(const QString& contents, QWidget* parent) : QLineEdit(contents, parent)
{
    init();
}

TYLineEdit::TYLineEdit(QWidget* parent) : QLineEdit(parent)
{
    init();
}

TYLineEdit::TYLineEdit(const QString& contents, bool isZeroOK, bool canBeNegative, QWidget* parent)
{
    _isZeroOK = isZeroOK;
    _canBeNegative = canBeNegative;
    init();
}

void TYLineEdit::init()
{
    QDoubleValidator* validator = new QDoubleValidator();
    QLocale* locale = new QLocale(QLocale::C);
    locale->setNumberOptions(QLocale::RejectGroupSeparator);
    validator->setLocale(*locale);

    validator->setNotation(QDoubleValidator::StandardNotation);

    if (!_canBeNegative)
    {
        if (_isZeroOK)
        {
            validator->setBottom(0.0);
        }
        else
        {
            validator->setBottom(0.0001);
        }
    }
    setValidator(validator);
    connect();
}

void TYLineEdit::connect()
{
    QObject::connect(this, &QLineEdit::textChanged, this, &TYLineEdit::adjustTextColor);
}

void TYLineEdit::keyPressEvent(QKeyEvent* evt)
{

    if (evt->key() == Qt::Key_Comma)
    {
        evt = new QKeyEvent(QEvent::KeyPress, Qt::Key_Period, Qt::NoModifier, 0, 0, 0, ".");
    }

    QLineEdit::keyPressEvent(evt);
}

void TYLineEdit::adjustTextColor()
{
    if (!this->hasAcceptableInput())
    {
        this->setStyleSheet("QLineEdit { color: red;}");
    }
    else
    {
        this->setStyleSheet("QLineEdit { color: black;}");
    }

    // Validate if parent or grand parent (case of Tabs) is TYFormDialog
    TYFormDialog* formDialog = nullptr;
    QObject* parent = this->parent();
    formDialog = dynamic_cast<TYFormDialog*>(parent);
    while (formDialog == nullptr && parent != nullptr)
    {
        parent = parent->parent();
        formDialog = dynamic_cast<TYFormDialog*>(parent);
    }

    if (formDialog != nullptr)
    {
        formDialog->validate();
    }
}
