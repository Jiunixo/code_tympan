/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * \file TYLineEdit.h
 * \brief outil IHM pour une entrée utilisateur (fichier header)
 * \author Projet_Tympan
 *
 */

#ifndef __TY_LINE_EDIT__
#define __TY_LINE_EDIT__

#include <QLineEdit>
#include <QRegExpValidator>
#include <QPushButton>

class TYLineEdit;

class TYLineEdit : public QLineEdit
{
    Q_OBJECT

public:
    /**
     * Constructor
     */
    TYLineEdit(const QString& contents, bool isZeroOK = true, bool canBeNegative = true,
               QWidget* parent = nullptr);
    /**
     * Constructor
     */
    TYLineEdit(const QString& contents, QWidget* parent = nullptr);
    /**
     * Constructor
     */
    TYLineEdit(QWidget* parent = nullptr);

    /**
     * Initialize the widget
     */
    void init();

    /**
     * overload the QLineEdit method
     * If the key pressed is a coma, replace it with a point
     */
    void keyPressEvent(QKeyEvent* evt);

    /**
     * Connect the signal'textChanged' with the slot 'adjustTextColor'
     */
    void connect();

public slots:
    /**
     * If the text is not a double with at most 2 decimal numbers, it is colored red
     */
    void adjustTextColor();

private:
    bool _isZeroOK = true;
    bool _canBeNegative = true;
};

#endif