/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/**
 * \file TYFormDialog.cpp
 * \brief Parent class of Tympan Qt dialogs of type form (body file)
 * \author Projet_Tympan
 *
 */

#include <QLineEdit>

#include "Tympan/gui/widgets/TYLineEdit.h"

#include "TYFormDialog.h"

TYFormDialog::TYFormDialog(QWidget* parent, Qt::WindowFlags f) : QDialog(parent, f) {}

bool TYFormDialog::validate()
{
    bool isValidated = true;
    QPushButton* pDefaultButton = nullptr;
    QLineEdit* pObject = nullptr;
    QObjectList objectsList = children();

    // Search default button
    for (int i = 0; i < objectsList.size(); i++)
    {
        pDefaultButton = dynamic_cast<QPushButton*>(objectsList[i]);
        if (pDefaultButton != nullptr && pDefaultButton->isDefault())
        {
            break;
        }
    }

    // If there is a default button then validate
    if (pDefaultButton != nullptr)
    {
        isValidated = validateChildren(this);
        pDefaultButton->setEnabled(isValidated);
    }
    else
    {
        isValidated = false;
    }
    return isValidated;
}

bool TYFormDialog::validateChildren(QObject* object)
{
    bool ret = true;

    QLineEdit* pQLineEdit = nullptr;
    pQLineEdit = dynamic_cast<QLineEdit*>(object);
    // If it is a QLineEdit, then validate it
    if (pQLineEdit != nullptr)
    {
        ret = ret && (pQLineEdit->hasAcceptableInput() || !pQLineEdit->isEnabled());
    }
    else
    // Else validate its children
    {
        QObject* pObject = nullptr;
        QObjectList objectsList = object->children();
        for (int i = 0; i < objectsList.size(); i++)
        {
            ret = ret && validateChildren(objectsList[i]);
        }
    }
    return ret;
}