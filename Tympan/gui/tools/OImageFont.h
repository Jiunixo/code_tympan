/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

/*
 *
 *
 *
 *
 */

#ifndef __O_IMAGEGLF__
#define __O_IMAGEGLF__

#include "OImage.h"
#include <map>
#include <vector>

class OImageFont : public OImage
{
public:
    struct OGLFontChar
    {
        int x;
        int y;
        int w;
        int h;
        int advance;
    };

public:
    OImageFont();
    virtual ~OImageFont();

    /**
     * Overloading of OImage::load
     */
    virtual bool load(const std::string& filename);

    /**
     * returns a constant reference to the character
     */
    const OGLFontChar& getChar(unsigned char c) const;

    /**
     * returns the kerning between the first and second characters
     */
    const int getKerning(unsigned char first, unsigned char second) const;

private:
    std::vector<OGLFontChar> _char;
    int _kernings[256][256] = {0};
    OGLFontChar _invalid_char;
};

#endif // __O_IMAGEGLF__
