/*
 * Copyright (C) <2012> <EDF-R&D> <FRANCE>
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.
 * See the GNU General Public License for more details.
 * You should have received a copy of the GNU General Public License along
 * with this program; if not, write to the Free Software Foundation, Inc.,
 * 51 Franklin Street, Fifth Floor, Boston, MA 02110-1301 USA.
 */

#ifndef __TY_CHEMIN_9613__
#define __TY_CHEMIN_9613__

#include <deque>
#include "TYEtape.h"
#include "Tympan/models/common/atmospheric_conditions.h"
#include "Tympan/models/common/acoustic_path.h"

/**
 * \file TYChemin.h
 * \brief Representation of one of the most optimal path between source and receptor: S--->R
 * \author Projet_Tympan
 * \version v1.1
 */

enum class TYTypeChemin
{
    CHEMIN_DIRECT,
    CHEMIN_SOL,
    CHEMIN_ECRAN,
    CHEMIN_REFLEX
};

::std::ostream& operator<<(::std::ostream& out, const TYTypeChemin& value);

enum class TYTypeAttenuation
{
    DIRECTIVITY_INDEX,
    ATTENUATION_ATM,
    ATTENUATION_GND_S,
    ATTENUATION_GND_R,
    ATTENUATION_GND_M,
    DZ_TOP,
    DZ_LEFT,
    DZ_RIGHT,
    ATTENUATION_BAR_TOP,
    ATTENUATION_BAR_LEFT,
    ATTENUATION_BAR_RIGHT,
    ATTENUATION_BAR,
    ATTENUATION_REFLEX
};

struct TYAttenuation
{
    TYTypeAttenuation type;
    OSpectreOctave attenuation;
};

/**
 * \class TYChemin
 *  \brief Representation of one of the most optimal path between source and receptor: S--->R.
 *  The class TYChemin represents a path between a Source and a receptor (Recepteur class). It's constituted
 * of a collection of steps (TYEtape class).
 */
class TYChemin
{
    // Methods
public:
    /**
     * \fn TYChemin()
     *\brief Constructor
     */
    TYChemin();
    /**
     * \fn TYChemin(const TYChemin& other)
     *\brief Copy contructor
     */
    TYChemin(const TYChemin& other);
    /**
     * \fn virtual ~TYChemin()
     * \brief Destructor
     */
    virtual ~TYChemin();

    /// Operator =.
    TYChemin& operator=(const TYChemin& other);
    /// Operator ==.
    bool operator==(const TYChemin& other) const;
    /// Operator !=.
    bool operator!=(const TYChemin& other) const;

    /**
     * \fn void calcAttenuation(const TYTabEtape& tabEtapes, const AtmosphericConditions & atmos)
     * \brief Compute the global attenuation on the path
     * @param [in] tabEtapes Array of steps.
     * @param [in] atmos Atmospheric ocnditions.
     * @param [in] dp Distance 2D between source and receptor for considered path.
     * @param [in] hs Height of source.
     * @param [in] hr Height of receptor
     * @param [in] Gs Ground factor of the source zone.
     * @param [in] Gm Ground factor of the middle zone.
     * @param [in] Gr Ground factor of the receptor zone.
     *
     */
    void calcAttenuation(const TYTabEtape& tabEtapes, const AtmosphericConditions& atmos, double dp = 0.0,
                         double hs = 0.0, double hr = 0.0, double Gs = 0.5, double Gm = 0.5, double Gr = 0.5);

    /**
     * \fn void computeBarAttenuation(const OSpectreOctave& Dz, const bool vertical, const bool left)
     * \ brief compute barrier attenuation on the path
     */
    void computeBarAttenuation(const OSpectreOctave& Dz, const bool vertical, const bool left);

    /**
     * \fn OSpectreOctave& getAttenuation(const TYTypeAttenuation& type)
     *     const OSpectreOctave& getAttenuation(const TYTypeAttenuation& type)
     * \brief Return attenuation of the path of the type
     * \return _attenuations[type]
     */
    OSpectreOctave& getAttenuation(TYTypeAttenuation type);

    // const OSpectreOctave& getAttenuation(TYTypeAttenuation type) const
    // {
    //     return _attenuations[type];
    //}

    // Kept for compatibility
    // TODO delete when 9613 is over
    OSpectreOctave& getAttenuation()
    {
        return _attenuation;
    }

    const OSpectreOctave& getAttenuation() const
    {
        return _attenuation;
    }

    /**
     * \fn void setAttenuation (const OSpectreOctave& att)
     * \brief Set the atmospheric attenuation
     */
    void setAttenuation(const TYTypeAttenuation& type, const OSpectreOctave& att)
    {
        _attenuations[type] = att;
    }

    /**
     * \fn void setAttenuationBarWhenNoPath(bool vertical, bool left)
     * \brief Set attenuation bar to max to traduce the lack of diffracted ray on this path
     */
    void setAttenuationBarWhenNoPath(bool vertical, bool left);

    /**
     * \fn double getLongueur()
     *     const double getLongueur()
     *     void setLongueur(const double & longueur)
     *\brief Get/Set the path length
     *\return _longueur
     */
    double getLongueur()
    {
        return _longueur;
    }
    const double getLongueur() const
    {
        return _longueur;
    }

    void setLongueur(const double& longueur)
    {
        _longueur = longueur;
    }

    /**
     * \fn   double getDistance()
     *       const double getDistance()
     *       void setDistance(const double & distance)
     * \brief Get/Set the distance between source and receptor
     *\return _distance
     *
     */
    double getDistance()
    {
        return _distance;
    }
    const double getDistance() const
    {
        return _distance;
    }

    void setDistance(const double& distance)
    {
        _distance = distance;
    }

    /**
     *\fn void setType(const TYTypeChemin& type)
     *\brief Change the path type
     */
    void setType(const TYTypeChemin& type)
    {
        _typeChemin = type;
    }

    /**
     * \fn TYTypeChemin getType()
     * \brief Return the path type
     * \return _typeChemin
     */
    const TYTypeChemin getType() const
    {
        return _typeChemin;
    }
    // Members
    /*!
     * \fn void build_eq_path();
     * \brief build an acoustic_path from the tab of etapes
     */
    void build_eq_path(const TYTabEtape& tabEtapes);
    acoustic_path* get_ray(OPoint3D ptR);

protected:
    /// Path type (has an influence on the algorithm)
    TYTypeChemin _typeChemin;

    /// Total path length
    double _longueur;

    /// Direct distance between source and receptor
    double _distance;

    /// Attenuations spectra of the path
    std::map<TYTypeAttenuation, OSpectreOctave> _attenuations;

    // Kept for compatibility
    // TODO delete when 9613 is over
    OSpectreOctave _attenuation;

    /// Equivalent acoustic_path
    acoustic_path* _eq_path;

private:
    void calcGroundAttenuations(double distance, double hs, double hr, double Gs, double Gm, double Gr);
    OSpectreOctave calcGroundAttenuationSR(double dp, double h, double G);
    OSpectreOctave calcGroundAttenuationM(double q, double Gm);
};

/// TYChemin collection
typedef std::deque<TYChemin> TYTabChemin;

#endif // __TY_CHEMIN_9613__
