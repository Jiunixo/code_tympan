import unittest

from utils import TympanTC, _test_solve_with_file


class TestSolve9613_DIFFRACTION_TU17_BIS_17534(TympanTC):
    def test_9613_diffraction_TU17_bis_17534(self):
        _test_solve_with_file("TEST_9613_DIFFRACTION_TU17_BIS_17534_NO_RESU.xml", self)


if __name__ == "__main__":
    unittest.main()
