import unittest

from utils import TympanTC, _test_solve_with_file


class TestSolveConvexHullFinderCase1(TympanTC):
    def test_convex_hull_finder_case_1(self):
        _test_solve_with_file("TEST_CONVEX_HULL_FINDER_CASE_1_NO_RESU.xml", self)


if __name__ == "__main__":
    unittest.main()
