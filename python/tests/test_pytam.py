import os.path as osp
import unittest
import numpy as np
from numpy.testing import assert_array_equal, assert_allclose

from utils import TympanTC
from tympan.models.solver import Model

_HERE = osp.realpath(osp.dirname(__file__))


class TestPyTam(TympanTC):
    def test_hierarchy(self):
        project = self.load_project(
            "projects-panel", "10_PROJET_SITE_emprise_non_convexe_avec_butte_et_terrains.xml"
        )
        site = project.site
        childs = site.childs()
        for c in childs:
            self.assertRegex(c.name, "Infrastructure|Topographie")

    def test_base(self):
        project = self.load_project("solver_export", "base.xml")
        model = Model.from_project(project)
        nodes = np.array(
            [
                [-100, 0, 0],
                [-100, -100, 0],
                [100, -100, 0],
                [100, 100, 0],
                [-50, 100, 0],
                [-50, 0, 0],
                [25, 50, 0],
                [100, 0, 0],
                [9.9, 100, 0],
                [16.8, -44.5, 0],
                [0, -100, 0],
            ]
        )
        triangles = np.array(
            [
                [9, 5, 10],
                [7, 6, 9],
                [5, 0, 1],
                [3, 6, 7],
                [6, 5, 9],
                [6, 3, 8],
                [2, 7, 9],
                [4, 0, 5],
                [4, 6, 8],
                [4, 5, 6],
                [5, 1, 10],
                [2, 9, 10],
            ]
        )
        self.assertEqual(model.npoints, 11)
        self.assertEqual(model.ntriangles, 12)
        self.assertEqual(model.nmaterials, 2)
        assert_allclose(np.array([model.node_coords(n) for n in range(11)]), nodes, rtol=0, atol=0.1)
        self.assertEqual(
            [t.material_name for t in model.triangles],
            [
                "Sol 0",
                "Sol 2",
                "Sol 0",
                "Sol 2",
                "Sol 2",
                "Sol 0",
                "Sol 2",
                "Sol 0",
                "Sol 0",
                "Sol 0",
                "Sol 0",
                "Sol 0",
            ],
        )
        assert_array_equal(np.array([t.nodes for t in model.triangles]), triangles)


if __name__ == "__main__":
    unittest.main()
