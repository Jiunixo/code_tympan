import sys
import os
from contextlib import contextmanager

from ._common import Spectrum  # noqa


@contextmanager
def filter_output(verbose=False, to=os.devnull, err_to=None):
    """
    This ContextManager redirects outputs to sys.stdout (standard output) to os.devnull
    (that is 'nul' device under Windows and /dev/null device under Linux).
    This is equivalent to filtering these outputs.
    @param verbose: False filters output, True does not (is the same as without filter_output)
    @param to: Where to redirect outputs
    @param err_to: Where to redirect errors
    @return: None
    """
    if verbose:
        yield
        return
    if err_to is None:
        err_to = to
    with _stdout_redirected(to=to, stdout=sys.stdout):
        with _stdout_redirected(to=err_to, stdout=sys.stderr):
            yield


def _fileno(file_or_fd):
    """
    Return the file descriptor of a file or of a file descriptor
    @param file_or_fd:
    @return: file descriptor
    """
    fd = getattr(file_or_fd, "fileno", lambda: file_or_fd)()
    if not isinstance(fd, int):
        raise ValueError("Expected a file (`.fileno()`) or a file descriptor")
    return fd


@contextmanager
def _stdout_redirected(to=os.devnull, stdout=None):
    """
    low level output redirection, see
    https://stackoverflow.com/questions/4675728/redirect-stdout-to-a-file-in-python/22434262#22434262
    @param to: Device toward which outputs must be redirected to
    @param stdout: Device stdout whose outputs must be redirected
    @return: None
    """
    if stdout is None:
        stdout = sys.stdout

    stdout_fd = _fileno(stdout)
    # copy stdout_fd before it is overwritten
    with os.fdopen(os.dup(stdout_fd), "wb") as copied:
        stdout.flush()  # flush library buffers that dup2 knows nothing about
        try:
            os.dup2(_fileno(to), stdout_fd)  # $ exec >&to
        except ValueError:  # filename
            with open(to, "ab") as to_file:
                os.dup2(to_file.fileno(), stdout_fd)  # $ exec > to
        try:
            yield stdout  # allow code to be run with the redirected stdout
        finally:
            stdout.flush()
            os.dup2(copied.fileno(), stdout_fd)  # $ exec >&copied
