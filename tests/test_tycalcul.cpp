/*
 * Copyright (C) <2022> <EDF-DTG> <France>
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

/**
 *
 * @brief Functional tests of the TYCalcul class
 *
 *  Created on: september 28, 2022
 *  Author: Jean Ripolles <jripolles@yahoo.fr>
 *
 */
#include "gtest/gtest.h"

#include "testutils.h"
#include "TympanTestsConfig.h"

typedef BuildingFromSiteFixture TYCalculfromXML;

/**
 * Testing the TYCalcul::fromXML method when loading a T42 project in
 * version 4.3 of Code_TYMPAN
 */
TEST_F(TYCalculfromXML, fromXMLConvertT42ToT43)
{
    std::string filename = tympan::path_to_test_data("T42_model_SR_Keep_Rays_True.xml");
    load_file(filename.c_str());
    ASSERT_TRUE(project);
    LPTYCalcul calcul = project->getCurrentCalcul();
    QString solverParams = calcul->solverParams;
    QString refSolverParams = QString(
        "[ACOUSTICRAYTRACER]\nAccelerator=3\nAngleDiffMin=5.0\nCylindreThick=0.3\nDiffractionDropDownNbRays="
        "False\nDiffractionFilterRayAtCreation=True\nDiffractionUseDistanceAsFilter="
        "True\nDiffractionUseRandomSampler=False\nDiscretization=2\nKeepDebugRay=False\nMaxDiffraction="
        "2\nMaxLength=5000.0\nMaxPathDifference=25.0\nMaxProfondeur=10\nMaxReflexion=2\nMaxTreeDepth="
        "12\nNbRayWithDiffraction=0\nNbRaysPerSource=100000\nRayTracingOrder=0\nSizeReceiver=2."
        "0\nUsePathDifValidation=False\nUsePostFilters=True\nUseSol=True\n[ANALYTICRAYTRACER]\nAnalyticDMax="
        "3000.0\nAnalyticH=0.1\nAnalyticNbRay=20\nAnalyticTMax=10.0\nCurveRaySampler=1\nFinalAnglePhi=360."
        "0\nFinalAngleTheta=0.0\nInitialAnglePhi=0.0\nInitialAngleTheta=0.0\n[DEBUG]"
        "\nDebugUseCloseEventSelector=True\nDebugUseDiffractionAngleSelector="
        "True\nDebugUseDiffractionPathSelector=True\nDebugUseFaceSelector=True\nDebugUseFermatSelector="
        "True\n[DEFAULTSOLVER]\nAngleDefavorable=45.0\nAngleFavorable=45.0\nDSWindDirection=0\nH1parameter="
        "10.0\nKeepRays=True\nModSummation=False\nNbThreads=4\nPropaConditions=0\nUseLateralDiffraction="
        "True\nUseRealGround=True\nUseReflection=True\nUseScreen=True\n[GEOMTRANSFORMER]"
        "\nAnalyticTypeTransfo=1\nMeshElementSizeMax=0.0\nRefineMesh=True\nUseVolumesLandtake="
        "False\nshowScene=False\n[METEO]\nAnalyticGradC=0.0\nAnalyticGradV=0.0\nAtmosHygrometry=70."
        "0\nAtmosPressure=101325.0\nAtmosTemperature=20.0\nWindDirection=0.0\n[PREPROCESSING]\nMinSRDistance="
        "0.3\n");
    EXPECT_EQ(solverParams, refSolverParams);
}

/**
 * Testing the TYCalcul::fromXML method when loading a T310 project in
 * version 4.3 of Code_TYMPAN
 */
TEST_F(TYCalculfromXML, fromXMLConvertT310ToT43)
{
    std::string filename = tympan::path_to_test_data("T310_model_SR.xml");
    load_file(filename.c_str());
    ASSERT_TRUE(project);
    LPTYCalcul calcul = project->getCurrentCalcul();
    QString solverParams = calcul->solverParams;
    QString refSolverParams = QString(
        "[ACOUSTICRAYTRACER]\nAccelerator=3\nAngleDiffMin=5.0\nCylindreThick=0.3\nDiffractionDropDownNbRays="
        "False\nDiffractionFilterRayAtCreation=True\nDiffractionUseDistanceAsFilter="
        "True\nDiffractionUseRandomSampler=False\nDiscretization=2\nKeepDebugRay=False\nMaxDiffraction="
        "2\nMaxLength=5000.0\nMaxPathDifference=25.0\nMaxProfondeur=10\nMaxReflexion=2\nMaxTreeDepth="
        "12\nNbRayWithDiffraction=0\nNbRaysPerSource=100000\nRayTracingOrder=0\nSizeReceiver=2."
        "0\nUsePathDifValidation=False\nUsePostFilters=True\nUseSol=True\n[ANALYTICRAYTRACER]\nAnalyticDMax="
        "3000.0\nAnalyticH=0.1\nAnalyticNbRay=20\nAnalyticTMax=10.0\nCurveRaySampler=1\nFinalAnglePhi=360."
        "0\nFinalAngleTheta=0.0\nInitialAnglePhi=0.0\nInitialAngleTheta=0.0\n[DEBUG]"
        "\nDebugUseCloseEventSelector=True\nDebugUseDiffractionAngleSelector="
        "True\nDebugUseDiffractionPathSelector=True\nDebugUseFaceSelector=True\nDebugUseFermatSelector="
        "True\n[DEFAULTSOLVER]\nAngleDefavorable=45.0\nAngleFavorable=45.0\nDSWindDirection=0\nH1parameter="
        "10\nKeepRays=False\nModSummation=False\nNbThreads=4\nPropaConditions=0\nUseLateralDiffraction="
        "True\nUseRealGround=True\nUseReflection=False\nUseScreen=True\n[GEOMTRANSFORMER]"
        "\nAnalyticTypeTransfo=1\nMeshElementSizeMax=0.0\nRefineMesh=True\nUseVolumesLandtake="
        "False\nshowScene=False\n[METEO]\nAnalyticGradC=0.0\nAnalyticGradV=0.0\nAtmosHygrometry=70"
        "\nAtmosPressure=101325\nAtmosTemperature=20\nWindDirection=0.0\n[PREPROCESSING]\nMinSRDistance="
        "0.3\n");
    EXPECT_EQ(solverParams, refSolverParams);
}
