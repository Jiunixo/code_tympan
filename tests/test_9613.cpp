﻿/**
 *
 * @brief Functional tests of 9613 Solver
 *
 *  Created on: october 27, 2022
 *  Author: Jean Ripolles <jean.ripolles@edf.fr>
 *
 */
#include "gtest/gtest.h"
#include "Tympan/solvers/9613Solver/TYAcousticModel.h"
#include "Tympan/solvers/9613Solver/TYSolver.h"
#include "Tympan/models/common/atmospheric_conditions.h"
#include "Tympan/models/solver/config.h"
#include "Tympan/models/common/plan.h"
#include <Tympan/solvers/9613Solver/TYFaceSelector.h>
#include "Tympan/models/common/mathlib.h"
#include <iostream>

/**
 * @brief Fixture which provides the T01 test case of 17534 norma for direct path
 */
class T01Test : public ::testing::Test
{
protected:
    void SetUp() override
    {
        distance = 194.19;
        /// Data setup:
        pointSource.setCoords(10, 10, 1);
        pointReceptor.setCoords(200, 50, 4);
        rayonSR._ptA = pointSource;
        rayonSR._ptB = pointReceptor;
        spectreSource = OSpectre::makeOctSpect();
        spectreSource.setType(SPECTRE_TYPE_LW);
        spectreSource.setDefaultValue(93);

        OVector3D directivityVector;
        tympan::SourceDirectivityInterface* directivity = new tympan::SphericalSourceDirectivity();
        directivityVector.setCoords(1, 1, 1);

        tympan::AcousticSource acousticSource =
            tympan::AcousticSource(pointSource, spectreSource, directivity);
        tympan::AcousticReceptor acousticReceptor = tympan::AcousticReceptor(pointReceptor);

        m_acousticSourceList.push_back(acousticSource);
        m_acousticReceptorList.push_back(acousticReceptor);

        _atmos_test = AtmosphericConditions(101325., 20., 70.);

        // Update path distance and length
        tabEtape01.clear();
        cheminDirect.setType(TYTypeChemin::CHEMIN_DIRECT);
        cheminDirect.setDistance(distance);
        cheminDirect.setLongueur(distance);

        // Update step
        TYEtape etape01;
        etape01.setType(TYSOURCE);
        etape01.setPoint(pointSource);
        etape01._Absorption = (acousticSource.directivity->lwAdjustment(OVector3D(pointSource, pointReceptor),
                                                                        pointSource.distFrom(pointReceptor)));
        tabEtape01.push_back(etape01);
    }

    // void TearDown() override {}

    // Inersection array
    std::deque<TYSIntersection> m_tabIntersect;

    // Paths objects
    TYChemin cheminDirect;
    // Array of steps
    TYTabEtape tabEtape01;
    // Source and receptor points
    OPoint3D pointSource;
    OPoint3D pointReceptor;
    OSegment3D rayonSR;
    double distance;
    // Source
    OSpectre spectreSource;

    std::vector<tympan::AcousticSource> m_acousticSourceList;
    std::vector<tympan::AcousticReceptor> m_acousticReceptorList;

    AtmosphericConditions _atmos_test = AtmosphericConditions(101325., 20., 70.);
};

/**
 * @brief Fixture which provides an aouctic model for acoustic computation on a surfacic source
 */
class FaceTest : public ::testing::Test
{
protected:
    void init(const OCoord3D& sourceCoords, const OCoord3D& receptorCoords, const OVector3D& pSupportNormal,
              double dSpecificSize)
    {
        /// Data setup:
        sourcePoint = sourceCoords;
        receptorPoint = receptorCoords;
        rayonSR._ptA = sourcePoint;
        rayonSR._ptB = receptorPoint;
        distance = sourcePoint.distFrom(receptorPoint);
        spectreSource = OSpectre::makeOctSpect();
        spectreSource.setType(SPECTRE_TYPE_LW);
        spectreSource.setDefaultValue(93);

        double specificSize = dSpecificSize;
        supportNormal = pSupportNormal;
        tympan::SourceDirectivityInterface* directivity =
            new tympan::VolumeFaceDirectivity(supportNormal, specificSize);

        tympan::AcousticSource acousticSource =
            tympan::AcousticSource(sourcePoint, spectreSource, directivity);
        tympan::AcousticReceptor acousticReceptor = tympan::AcousticReceptor(receptorPoint);

        m_acousticSourceList.push_back(acousticSource);
        m_acousticReceptorList.push_back(acousticReceptor);

        _atmos_test = AtmosphericConditions(101325., 20., 70.);

        // Update path distance and length
        tabEtape01.clear();
        cheminDirect.setType(TYTypeChemin::CHEMIN_DIRECT);
        cheminDirect.setDistance(distance);
        cheminDirect.setLongueur(distance);

        // Update step
        TYEtape etape01;
        etape01.setType(TYSOURCE);
        etape01.setPoint(sourcePoint);
        etape01._Absorption = (acousticSource.directivity->lwAdjustment(OVector3D(sourcePoint, receptorPoint),
                                                                        sourcePoint.distFrom(receptorPoint)));
        tabEtape01.push_back(etape01);
    }

    void buildSolver()
    {
        tympan::AcousticProblemModel newProblem;
        tympan::AcousticResultModel newResult;
        tympan::LPSolverConfiguration configuration = tympan::SolverConfiguration::get();
        configuration->AtmosPressure = 101325.;
        configuration->AtmosTemperature = 20.;
        configuration->AtmosHygrometry = 70.;

        TYSolver mSolver;

        // Necessary stuff to define acoustic problem
        tympan::node_idx x_inter, y_inter, z_inter;
        tympan::triangle_idx idx_triIntersect;

        // Add ground triangle to acoustic problem
        x_inter = newProblem.make_node(-1000, 0, 0); // node[0]
        y_inter = newProblem.make_node(1000, 0, 0);  // node[1]
        z_inter = newProblem.make_node(0, 2000, 0);  // node[2]

        idx_triIntersect = newProblem.make_triangle(x_inter, y_inter, z_inter);

        boost::shared_ptr<tympan::AcousticMaterialBase> mat02(
            new tympan::AcousticGroundMaterial("sol", 20000, 1, 500, 0.5));

        newProblem.triangle(idx_triIntersect).made_of.operator=(mat02);

        // Build TYTrajet object and link with tabChemin
        pTrajet = new TYTrajet{m_acousticSourceList[0], m_acousticReceptorList[0]};
        TYTabChemin& tabChemin = pTrajet->getChemins();

        mSolver.solve(newProblem, newResult, configuration);
        // Construction du rayon SR
        OSegment3D rayon;
        pTrajet->getPtSetPtRfromOSeg3D(rayon);

        mSolver.getFaceSelector()->selectFaces(m_tabIntersect, rayon, m_acousticSourceList[0].volume_id);
        pModelAcoustic = new TYAcousticModel(mSolver);
        pModelAcoustic->init();

        pModelAcoustic->computeCheminSansEcran(m_tabIntersect, rayonSR, m_acousticSourceList[0], tabChemin,
                                               rayonSR.longueur());
    }

    void TearDown() override
    {
        delete pModelAcoustic;
        pModelAcoustic = nullptr;

        delete pTrajet;
        pTrajet = nullptr;
    }

    // Inersection array
    std::deque<TYSIntersection> m_tabIntersect;

    // Route object
    TYTrajet* pTrajet;

    // Paths objects
    TYChemin cheminDirect;
    // Array of steps
    TYTabEtape tabEtape01;
    // Source and receptor points
    OPoint3D sourcePoint;
    OPoint3D receptorPoint;
    OSegment3D rayonSR;
    double distance;
    // Source
    OSpectre spectreSource;
    OVector3D supportNormal;
    double specificSize = 0.0;

    std::vector<tympan::AcousticSource> m_acousticSourceList;
    std::vector<tympan::AcousticReceptor> m_acousticReceptorList;

    AtmosphericConditions _atmos_test = AtmosphericConditions(101325., 20., 70.);

    TYAcousticModel* pModelAcoustic;
};

// Testing TYAcousticModel::solve method for T01 test case
TEST_F(T01Test, solve)
{
    // Result computed object
    OSpectre computedSpectreLp;

    // Expected result object
    OSpectre expectedSpectreLp = OSpectre::makeOctSpect();
    expectedSpectreLp.setValue(16.0, 39.92);
    expectedSpectreLp.setValue(20.0, 39.90);
    expectedSpectreLp.setValue(25.0, 39.86);
    expectedSpectreLp.setValue(31.5, 39.70);
    expectedSpectreLp.setValue(40.0, 39.37);
    expectedSpectreLp.setValue(50.0, 38.95);
    expectedSpectreLp.setValue(63.0, 38.17);
    expectedSpectreLp.setValue(80.0, 35.47);
    expectedSpectreLp.setValue(100.0, 25.04);

    EXPECT_NEAR(194.19, rayonSR.longueur(), 0.01);
    EXPECT_EQ(1, tabEtape01.size());

    // Build solver
    tympan::AcousticProblemModel newProblem;
    tympan::AcousticResultModel newResult;
    tympan::LPSolverConfiguration configuration = tympan::SolverConfiguration::get();
    configuration->AtmosPressure = 101325.;
    configuration->AtmosTemperature = 20.;
    configuration->AtmosHygrometry = 70.;

    TYSolver mSolver;

    // Necessary stuff to define acoustic problem
    tympan::node_idx x_inter, y_inter, z_inter;
    tympan::triangle_idx idx_triIntersect;

    // Test with one triangle which and intercepts the ray while the other does not
    x_inter = newProblem.make_node(0, 0, 0);    // node[0]
    y_inter = newProblem.make_node(1000, 0, 0); // node[1]
    z_inter = newProblem.make_node(0, 1000, 0); // node[2]

    idx_triIntersect = newProblem.make_triangle(x_inter, y_inter, z_inter);

    boost::shared_ptr<tympan::AcousticMaterialBase> mat02(
        new tympan::AcousticGroundMaterial("sol", 20000, 1, 500, 0.0));

    newProblem.triangle(idx_triIntersect).made_of.operator=(mat02);

    // Build TYTrajet object and link with tabChemin
    TYTrajet mTrajet(m_acousticSourceList[0], m_acousticReceptorList[0]);
    TYTabChemin& tabChemin = mTrajet.getChemins();

    mSolver.solve(newProblem, newResult, configuration);
    // Construction du rayon SR
    OSegment3D rayon;
    mTrajet.getPtSetPtRfromOSeg3D(rayon);

    mSolver.getFaceSelector()->selectFaces(m_tabIntersect, rayon, m_acousticSourceList[0].volume_id);
    TYAcousticModel* modelAcoustic = new TYAcousticModel(mSolver);
    modelAcoustic->init();

    modelAcoustic->computeCheminSansEcran(m_tabIntersect, rayonSR, m_acousticSourceList[0], tabChemin,
                                          rayonSR.longueur());

    // Call to the tested method:
    modelAcoustic->solve(mTrajet);

    // Compare computed spectrum with expected one
    computedSpectreLp = mTrajet.getSpectre();
    computedSpectreLp = computedSpectreLp.toOct();

    for (unsigned int i = 0; i < expectedSpectreLp.getNbValues(); i++)
    {
        EXPECT_NEAR(expectedSpectreLp.getTabValReel()[i], computedSpectreLp.getTabValReel()[i], 0.01);
    }
}

// Testing path attenuation computation for T01 test
TEST_F(T01Test, calcAttenuation)
{
    // Computed attenuation spectrums
    OSpectreOctave computedSpectreAtmAtt;

    // Expected atmospheric attenuation spectrum
    OSpectreOctave expectedSpectreAtmAtt;
    expectedSpectreAtmAtt.setValue(31.5, 0.0);
    expectedSpectreAtmAtt.setValue(63.0, 0.02);
    expectedSpectreAtmAtt.setValue(125.0, 0.06);
    expectedSpectreAtmAtt.setValue(250.0, 0.21);
    expectedSpectreAtmAtt.setValue(500.0, 0.54);
    expectedSpectreAtmAtt.setValue(1000.0, 0.97);
    expectedSpectreAtmAtt.setValue(2000.0, 1.75);
    expectedSpectreAtmAtt.setValue(4000.0, 4.45);
    expectedSpectreAtmAtt.setValue(8000.0, 14.87);

    // Build TYTrajet object
    TYTrajet mTrajet(m_acousticSourceList[0], m_acousticReceptorList[0]);

    // Check number of steps
    EXPECT_EQ(1, tabEtape01.size());

    // Call to the tested method:
    cheminDirect.calcAttenuation(tabEtape01, _atmos_test);

    // Compare computed atmospheric attenuation spectrum with expected one
    computedSpectreAtmAtt = cheminDirect.getAttenuation(TYTypeAttenuation::ATTENUATION_ATM);
    for (unsigned int i = 0; i < 9; i++)
    {
        EXPECT_NEAR(expectedSpectreAtmAtt.getTabValReel()[i], computedSpectreAtmAtt.getTabValReel()[i], 0.01);
    }
}

TEST(test_TYAcoustiModel, computeEffectiveBarAttenuation)
{
    TYSolver m_Solver;
    TYAcousticModel m_AcousticModel(m_Solver);

    // Test Case TU08 ISO TR 17534-3
    const double Abar_top_values_TU08[]{4.92, 5.06, 4.82, 0.00, 0.26, 7.58, 9.84, 12.12, 14.71};
    const double Abar_right_values_TU08[]{28.24, 31.26, 34.27, 37.27, 40.28, 43.29, 46.30, 49.31, 52.32};
    const double Abar_left_values_TU08[]{26.93, 29.94, 32.94, 35.95, 38.96, 41.97, 44.98, 47.99, 51.00};

    const double Abar_expected_values_TU08[]{4.87, 5.04, 4.81, 0.00, 0.26, 7.58, 9.84, 12.12, 14.71};

    OSpectreOctave Abar_top_TU08{Abar_top_values_TU08, TY_SPECTRE_OCT_NB_ELMT, 0};
    OSpectreOctave Abar_right_TU08{Abar_right_values_TU08, TY_SPECTRE_OCT_NB_ELMT, 0};
    OSpectreOctave Abar_left_TU08{Abar_left_values_TU08, TY_SPECTRE_OCT_NB_ELMT, 0};

    OSpectreOctave Abar_computed_TU08 =
        m_AcousticModel.computeEffectiveBarAttenuation(Abar_top_TU08, Abar_right_TU08, Abar_left_TU08);

    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(Abar_expected_values_TU08[i], Abar_computed_TU08.getTabValReel()[i], 0.01);
    }

    // Test Case TU09 ISO TR 17534-3
    const double Abar_top_values_TU09[]{4.92, 5.07, 4.83, 0.00, 0.28, 7.61, 9.88, 12.16, 14.76};
    double Abar_right_values_TU09[]{17.22, 20.11, 23.05, 26.03, 29.03, 32.03, 35.03, 38.04, 41.05};
    double Abar_left_values_TU09[]{6.17, 7.21, 8.76, 10.80, 13.24, 15.93, 18.77, 21.69, 24.66};

    double Abar_expected_values_TU09[]{2.35, 2.91, 3.31, 0.00, 0.06, 7.00, 9.34, 11.69, 14.32};

    OSpectreOctave Abar_top_TU09{Abar_top_values_TU09, TY_SPECTRE_OCT_NB_ELMT, 0};
    OSpectreOctave Abar_right_TU09{Abar_right_values_TU09, TY_SPECTRE_OCT_NB_ELMT, 0};
    OSpectreOctave Abar_left_TU09{Abar_left_values_TU09, TY_SPECTRE_OCT_NB_ELMT, 0};

    OSpectreOctave Abar_computed_TU09 =
        m_AcousticModel.computeEffectiveBarAttenuation(Abar_top_TU09, Abar_right_TU09, Abar_left_TU09);

    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(Abar_expected_values_TU09[i], Abar_computed_TU09.getTabValReel()[i], 0.01);
    }
}

TEST(test_TYAcoustiModel, calculAttDiffraction)
{
    TYSolver m_Solver;
    TYAcousticModel m_AcousticModel(m_Solver);
    m_AcousticModel.init();

    OSegment3D ray{OPoint3D{10.0, 10.0, 1.0}, OPoint3D{200.0, 50.0, 4.0}};
    double distance = ray.longueur();
    double width = 0.3;

    // -----------------------------
    // Test Case TU08 ISO TR 17534-3
    // -----------------------------

    // Vertical path
    double diffracted_path_length = 194.32174234119347;
    double dss = 170.31491047660424;
    double dsr = 24.006831864589230;

    // Expected result
    const double Dz_top_expected_values_TU08[]{4.92, 5.06, 5.33, 5.83, 6.68, 8.01, 9.84, 12.12, 14.71};

    // Call tested method
    OSpectreOctave Dz_top_TU08 =
        m_AcousticModel.calculAttDiffraction(ray, diffracted_path_length, dss, dsr, width, true);

    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(Dz_top_expected_values_TU08[i], Dz_top_TU08.getTabValReel()[i], 0.01);
    }

    // Left lateral path
    diffracted_path_length = 461.71067014437926;
    dss = 246.99043718425938;
    dsr = 214.70992231884702;

    // Expected result
    // The commented expected values just below is the current ISO TR 17534-3 values
    // const double
    // Dz_left_expected_values_TU08[]{26.98, 29.94, 32.94, 35.95, 38.96, 41.97, 44.98, 47.99, 51.00};
    const double Dz_left_expected_values_TU08[]{26.98, 29.98, 32.94, 35.95, 38.96,
                                                41.97, 44.98, 47.99, 51.00};

    // Call tested method
    OSpectreOctave Dz_left_TU08 =
        m_AcousticModel.calculAttDiffraction(ray, diffracted_path_length, dss, dsr, width, false);

    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(Dz_left_expected_values_TU08[i], Dz_left_TU08.getTabValReel()[i], 0.01);
    }

    // Right lateral path
    diffracted_path_length = 557.03097891776429;
    dss = 318.02346651606138;
    dsr = 239.00751240170285;

    // Expected result
    // The commented expected values just below is the current ISO TR 17534-3 values
    // const double
    // Dz_right_expected_values_TU08[]{28.30, 31.26, 34.27, 37.27, 40.28, 43.29, 46.30, 49.31, 52.32};
    const double Dz_right_expected_values_TU08[]{28.30, 31.30, 34.27, 37.27, 40.28,
                                                 43.29, 46.30, 49.31, 52.32};

    // Call tested method
    OSpectreOctave Dz_right_TU08 =
        m_AcousticModel.calculAttDiffraction(ray, diffracted_path_length, dss, dsr, width, false);

    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(Dz_right_expected_values_TU08[i], Dz_right_TU08.getTabValReel()[i], 0.01);
    }

    // -----------------------------
    // Test Case TU09 ISO TR 17534-3
    // -----------------------------

    // Vertical path
    diffracted_path_length = 194.32310361085095;
    dss = 170.40787107580005;
    dsr = 23.611766892229703;

    // Expected result
    // The commented expected values just below is the current ISO TR 17534-3 values
    // const double Dz_top_expected_values_TU09[]{4.92, 5.07, 5.34, 5.84, 6.70, 8.04, 9.88, 12.16, 14.76};
    const double Dz_top_expected_values_TU09[]{4.92, 5.07, 5.35, 5.85, 6.72, 8.06, 9.91, 12.19, 14.79};

    // Call tested method
    OSpectreOctave Dz_top_TU09 =
        m_AcousticModel.calculAttDiffraction(ray, diffracted_path_length, dss, dsr, width, true);

    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(Dz_top_expected_values_TU09[i], Dz_top_TU09.getTabValReel()[i], 0.01);
    }

    // Left lateral path
    diffracted_path_length = 194.81088217216003;
    dss = 169.94843707776130;
    dsr = 24.862445094398741;

    // Expected result
    // The commented expected values just below is the current ISO TR 17534-3 values
    // const double
    // Dz_left_expected_values_TU09[]{6.17, 7.21, 8.76, 10.80, 13.24, 15.93, 18.77, 21.69, 24.66};
    const double Dz_left_expected_values_TU09[]{6.18, 7.25, 8.80, 10.85, 13.29, 15.98, 18.82, 21.75, 24.71};

    // Call tested method
    OSpectreOctave Dz_left_TU09 =
        m_AcousticModel.calculAttDiffraction(ray, diffracted_path_length, dss, dsr, width, false);

    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(Dz_left_expected_values_TU09[i], Dz_left_TU09.getTabValReel()[i], 0.01);
    }

    // Right lateral path
    diffracted_path_length = 221.32715590014462;
    dss = 179.88011212980598;
    dsr = 41.147008764659034;

    // Expected result
    // The commented expected values just below is the current ISO TR 17534-3 values
    // const double
    // Dz_right_expected_values_TU09[]{17.22, 20.11, 23.05, 26.03, 29.03, 32.03, 35.03, 38.04, 41.05};
    const double Dz_right_expected_values_TU09[]{17.27, 20.15, 23.07, 26.04, 29.04,
                                                 32.04, 35.05, 38.05, 41.06};

    // Call tested method
    OSpectreOctave Dz_right_TU09 =
        m_AcousticModel.calculAttDiffraction(ray, diffracted_path_length, dss, dsr, width, false);

    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(Dz_right_expected_values_TU09[i], Dz_right_TU09.getTabValReel()[i], 0.01);
    }
}

TEST(test_TYAcoustiModel, getGroundFactors)
{
    // Init acoustic model
    TYSolver solver;
    TYAcousticModel acousticModel(solver);
    acousticModel.init();

    // Prepare input data
    double hs = 1.0;
    double hr = 4.0;
    OSegment3D SO2D{OPoint3D{10.0, 10.0, 0.0}, OPoint3D{131.859115666847, 54.5497793504172, 0.0}};
    OSegment3D OR2D{OPoint3D{131.859115666847, 54.5497793504172, 0.0}, OPoint3D{200.0, 50.0, 0.0}};

    // Build intersection segments
    std::deque<TYSIntersection> tabIntersectUpSegment;
    std::deque<TYSIntersection> tabIntersectDownSegment;
    TYSIntersection intersection;

    // Build materials
    tympan::AcousticGroundMaterial* ground_A1 =
        new tympan::AcousticGroundMaterial{"A1", 20000.0, 0.0, 0.001, 0.9};
    tympan::AcousticGroundMaterial* ground_A2 =
        new tympan::AcousticGroundMaterial{"A2", 20000.0, 0.0, 0.001, 0.5};
    tympan::AcousticGroundMaterial* ground_A3 =
        new tympan::AcousticGroundMaterial{"A3", 20000.0, 0.0, 0.001, 0.2};
    tympan::AcousticBuildingMaterial* screen_mat =
        new tympan::AcousticBuildingMaterial("Ecran", OSpectreComplex(TYComplex(1.0, 0.0)));

    // Default horizontal segment (not used)
    OSegment3D segH{OPoint3D{0.0, 0.0, 0.0}, OPoint3D{0.0, 1.0, 0.0}};

    // Build intersection segments from source to reflexion point
    // Intersecting segment #12
    OSegment3D segV{
        OPoint3D{90.870228467705047, 39.564885766147476, 0.0},
        OPoint3D{120.0, 50.21427286526017, 0.0},
    };
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A2;
    tabIntersectUpSegment.push_back(intersection);

    // Intersecting segment #13
    segV = OSegment3D{OPoint3D{146.76736163869009, 60.0, 0.0},
                      OPoint3D{130.31251481015971, 53.984366360740168, 1.5865407400245704}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A2;
    tabIntersectUpSegment.push_back(intersection);

    // Intersecting segment #14
    segV = OSegment3D{OPoint3D{0.0, 6.3441570122490774, 0.0},
                      OPoint3D{30.389850558240507, 17.454209218463287, 0.0}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = true;
    intersection.material = ground_A1;
    tabIntersectUpSegment.push_back(intersection);

    // Intersecting segment #15
    segV = OSegment3D{OPoint3D{30.389850558240511, 17.454209218463284, 0.0},
                      OPoint3D{50.0, 24.623371951003705, 0.0}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A1;
    tabIntersectUpSegment.push_back(intersection);

    // Intersecting segment #16
    segV = OSegment3D{OPoint3D{120.0, 50.214272865260178, 0.0},
                      OPoint3D{130.31251481015971, 53.984366360740168, 1.5865407400245712}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A2;
    tabIntersectUpSegment.push_back(intersection);

    // Intersecting segment #17
    segV = OSegment3D{OPoint3D{50.0, 24.623371951003705, 0.0},
                      OPoint3D{90.870228467705061, 39.564885766147469, 0.0}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A2;
    tabIntersectUpSegment.push_back(intersection);

    // Build intersection segments from reflexion point to receptor
    // Intersecting segment #12
    segV = OSegment3D{OPoint3D{50.232551767803301, 60.0, 0.0}, OPoint3D{120.0, 55.341614679577745, 0.0}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A2;
    tabIntersectDownSegment.push_back(intersection);

    // Intersecting segment #13
    segV = OSegment3D{OPoint3D{129.01803646248800, 54.739478730215325, 1.3873902249981556},
                      OPoint3D{150.0, 53.338509174736096, 1.7568767011685020}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A2;
    tabIntersectDownSegment.push_back(intersection);

    // Intersecting segment #14
    segV = OSegment3D{OPoint3D{120.0, 55.341614679577738, 0.0},
                      OPoint3D{129.01803646248800, 54.739478730215332, 1.3873902249981547}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A2;
    tabIntersectDownSegment.push_back(intersection);

    // Intersecting segment #15
    segV = OSegment3D{OPoint3D{174.32713690318872, 51.714181779808975, 3.9511689821147100},
                      OPoint3D{179.67879398609779, 51.356850654382306, 9.1813529209381226}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A3;
    tabIntersectDownSegment.push_back(intersection);

    // Intersecting segment #16
    segV = OSegment3D{OPoint3D{201.82954544693868, 49.877840914795989, 10.0},
                      OPoint3D{195.0, 50.333850917473626, 10.0}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A3;
    tabIntersectDownSegment.push_back(intersection);

    // Intersecting segment #17
    segV = OSegment3D{OPoint3D{201.82954544693868, 49.877840914795989, 10.0},
                      OPoint3D{205.0, 49.666149082526410, 10.0}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A3;
    tabIntersectDownSegment.push_back(intersection);

    // Intersecting segment #18
    segV = OSegment3D{OPoint3D{210.0, 49.332298165052812, 0.0},
                      OPoint3D{207.65209128136641, 49.489068461024814, 4.6958174372671628}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A3;
    tabIntersectDownSegment.push_back(intersection);

    // Intersecting segment #19
    segV = OSegment3D{OPoint3D{205.0, 49.666149082526402, 10.0},
                      OPoint3D{207.65209128136644, 49.489068461024821, 4.6958174372671380}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A3;
    tabIntersectDownSegment.push_back(intersection);

    // Intersecting segment #20
    segV = OSegment3D{OPoint3D{158.74890513038827, 52.754343173802162, 3.4551584099392172},
                      OPoint3D{174.32713690318872, 51.714181779808975, 3.9511689821147113}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A3;
    tabIntersectDownSegment.push_back(intersection);

    // Intersecting segment #21
    segV = OSegment3D{OPoint3D{150.0, 53.338509174736096, 1.7568767011685016},
                      OPoint3D{158.74890513038827, 52.754343173802162, 3.4551584099392167}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A3;
    tabIntersectDownSegment.push_back(intersection);

    // Intersecting segment #22
    segV = OSegment3D{OPoint3D{179.67879398609779, 51.356850654382306, 9.1813529209381226},
                      OPoint3D{185.0, 51.001552752420842, 10.0}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A3;
    tabIntersectDownSegment.push_back(intersection);

    // Intersecting segment #23
    segV = OSegment3D{OPoint3D{188.77842996409203, 50.749266290396463, 10.0},
                      OPoint3D{195.0, 50.333850917473626, 10.0}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A3;
    tabIntersectDownSegment.push_back(intersection);

    // Intersecting segment #24
    segV = OSegment3D{OPoint3D{185.0, 51.001552752420842, 10.0},
                      OPoint3D{188.77842996409203, 50.749266290396463, 10.0}};
    intersection.isEcran = false;
    intersection.noIntersect = false;
    intersection.isInfra = false;
    intersection.segInter[0] = segV;
    intersection.segInter[1] = segH;
    intersection.bIntersect[0] = true;
    intersection.bIntersect[1] = false;
    intersection.material = ground_A3;
    tabIntersectDownSegment.push_back(intersection);

    // Expected values
    double Gs_expected{0.90}, Gr_expected{0.37}, Gm_expected{0.60};

    // Returned values to be tested
    double Gs{0.0}, Gr{0.0}, Gm{0.0};

    // Call tested method
    acousticModel.getGroundfactors(tabIntersectUpSegment, tabIntersectDownSegment, SO2D, OR2D, hs, hr, Gs, Gm,
                                   Gr);

    // Compare actual and expected values
    EXPECT_NEAR(Gs, Gs_expected, 0.01);
    EXPECT_NEAR(Gr, Gr_expected, 0.01);
    EXPECT_NEAR(Gm, Gm_expected, 0.01);
}

// Test surfacic directivity computed in TYAcousticModel
TEST(test_TYAcoustiModel, addStep)
{
    // Init acoustic model
    TYSolver solver;
    TYAcousticModel acousticModel(solver);
    acousticModel.init();

    // -- SURFACIC DIRECTIVITY --
    // Common intializations
    OPoint3D sourcePoint{0.5, 0.0, 2.0};
    OPoint3D receptorPoint;
    double specificSize = sqrt(2);
    OVector3D supportNormal{1.0, 0.0, 0.0};
    tympan::SourceDirectivityInterface* directivity =
        new tympan::VolumeFaceDirectivity(supportNormal, specificSize);
    tympan::SourceDirectivityInterface* baffled =
        new tympan::BaffledFaceDirectivity(supportNormal, specificSize);
    tympan::SourceDirectivityInterface* chimney =
        new tympan::ChimneyFaceDirectivity(supportNormal, specificSize);
    OSpectre spectreSource = OSpectre::makeOctSpect();
    spectreSource.setType(SPECTRE_TYPE_LW);
    spectreSource.setDefaultValue(93);

    tympan::AcousticSource acousticSource = tympan::AcousticSource(sourcePoint, spectreSource, directivity);
    tympan::AcousticSource acousticBaffled(sourcePoint, spectreSource, baffled);
    tympan::AcousticSource acousticChimney(sourcePoint, spectreSource, chimney);

    TYTabEtape Etapes;

    OSpectreOctave computedDirectivity;
    OSpectreOctave computedDirectivityBaffled;
    OSpectreOctave computedDirectivityChimney;

    // Directivity with receptor in front of surfacic source
    receptorPoint.setCoords(100.0, 0.0, 2.0);
    Etapes.clear();

    // Expected directivity
    const double expectedDirectivity_surfacic_front[]{1.66, 1.79, 1.88, 1.93, 1.96, 1.97, 1.98, 1.98, 1.99};

    // Call tested method
    acousticModel.addStep(sourcePoint, receptorPoint, acousticSource, true, Etapes);
    computedDirectivity = Etapes[0]._Absorption;
    Etapes.clear();

    acousticModel.addStep(sourcePoint, receptorPoint, acousticBaffled, true, Etapes);
    computedDirectivityBaffled = Etapes[0]._Absorption;
    Etapes.clear();

    acousticModel.addStep(sourcePoint, receptorPoint, acousticChimney, true, Etapes);
    computedDirectivityChimney = Etapes[0]._Absorption;
    Etapes.clear();

    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(expectedDirectivity_surfacic_front[i], computedDirectivity.getTabValReel()[i], 0.01);
    }

    // Directivity with receptor at right angle of surfacic source
    receptorPoint.setCoords(0.5, 100.0, 2.0);

    // Expected directivity
    const double expectedDirectivity_surfacic_right_angle[]{0.99, 0.99, 0.99, 0.99, 0.99,
                                                            0.99, 0.99, 0.99, 0.99};

    const double expectedDirectivity_baffled[]{2.94, 9.48, 8.96, 4.34, 3.35, 2.33, 2.33, 2.33, 2.33};

    const double expectedDirectivity_chimney[]{1.70, 3.85, 13.71, 16.91, 16.91, 16.91, 16.91, 16.91, 16.91};

    // Call tested method
    acousticModel.addStep(sourcePoint, receptorPoint, acousticSource, true, Etapes);

    computedDirectivity = Etapes[0]._Absorption;

    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(expectedDirectivity_surfacic_right_angle[i], computedDirectivity.getTabValReel()[i],
                    0.01);
    }
    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(expectedDirectivity_baffled[i], computedDirectivityBaffled.getTabValReel()[i], 0.01);
    }
    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(expectedDirectivity_chimney[i], computedDirectivityChimney.getTabValReel()[i], 0.01);
    }
}

// Testing TYAcousticModel::solve method for a surfacic source with a front directivity
TEST_F(FaceTest, solve_directivity_surfacic_front)
{
    // -- SURFACIC DIRECTIVITY --
    // Directivity with receptor in front of surfacic source
    // Init FaceTest
    OCoord3D sourceCoords{0.5, 0.0, 2.5};
    OCoord3D receptorCoords{100.0, 0.0, 2.0};
    OVector3D supportNormal{1.0, 0.0, 0.0};
    double specificSize = sqrt(2.0);
    init(sourceCoords, receptorCoords, supportNormal, specificSize);

    // Result computed object
    OSpectre computedSpectreLp;

    // Expected spectrum values
    const double expected_spectrum_surfacic_front[]{47.25, 47.56, 45.10, 41.58, 44.88,
                                                    45.93, 45.62, 44.24, 38.90};
    // Build solver
    buildSolver();

    // Call to the tested method:
    pModelAcoustic->solve(*pTrajet);

    // Compare computed spectrum with expected one
    computedSpectreLp = pTrajet->getSpectre();
    computedSpectreLp = computedSpectreLp.toOct();

    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(expected_spectrum_surfacic_front[i], computedSpectreLp.getTabValReel()[i], 0.01);
    }
}

// Testing TYAcousticModel::solve method for a surfacic source with a directivity at right angle
TEST_F(FaceTest, solve_directivity_surfacic_right_angle)
{
    // -- SURFACIC DIRECTIVITY --
    // Directivity with receptor at righ angle of surfacic source
    // Init FaceTest
    OCoord3D sourceCoords{0.5, 0.0, 2.5};
    OCoord3D receptorCoords{1.0, 100.0, 2.0};
    OVector3D supportNormal{1.0, 0.0, 0.0};
    double specificSize = sqrt(2.0);
    init(sourceCoords, receptorCoords, supportNormal, specificSize);

    // Result computed object
    OSpectre computedSpectreLp;

    // Expected spectrum values
    const double expected_spectrum_surfacic_right_angle[]{44.99, 44.98, 42.31, 38.67, 41.91,
                                                          42.93, 42.59, 41.20, 35.83};
    // Build solver
    buildSolver();

    // Call to the tested method:
    pModelAcoustic->solve(*pTrajet);

    // Compare computed spectrum with expected one
    computedSpectreLp = pTrajet->getSpectre();
    computedSpectreLp = computedSpectreLp.toOct();

    for (unsigned int i = 0; i < TY_SPECTRE_OCT_NB_ELMT; i++)
    {
        EXPECT_NEAR(expected_spectrum_surfacic_right_angle[i], computedSpectreLp.getTabValReel()[i], 0.01);
    }
}
